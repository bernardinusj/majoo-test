<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\produk;

class produkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produk = new produk;
        $produk->nama_produk = 'produk tes 1';
        $produk->deskripsi_produk = 'deskripsi produk test 1';
        $produk->harga_produk = 1000;
        $produk->id_kategori_produk = 1;
        $produk->path_gambar_produk = 'paket-advance.png';
        $produk->save();
    }
}
