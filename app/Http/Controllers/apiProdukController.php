<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
Use Exception;
use Illuminate\Support\Facades\Storage;

class apiProdukController extends Controller
{
    public function getAllProduk()
    {
        $data = DB::table('produks')->get();
        return json_decode($data);
    }

    public function getProduk($id)
    {
        $data = DB::table('produks')
                ->where('id',$id)
                ->get();
        return json_decode($data);
    }

    public function editData(Request $request)
    {
        $bodyContent = $request->all();
        $image_64 = $request['gambar'];
        $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
        $image = str_replace($replace, '', $image_64); 
        $image = str_replace(' ', '+', $image); 
        $imageName =  $request['fileName'];

        Storage::disk('public')->put($imageName, base64_decode($image));
        $validator = Validator::make($bodyContent,[
            'id' => 'required',
            'namaProduk' => 'required',
            'deskripsiProduk' => 'required',
            'harga' => 'required',
            'gambar' => 'required',
            'fileName' => 'required',
        ]);
        if ($validator->fails()) {
            $response = response()->json([
               'success'=> false,
               'message'=> $validator->errors()->all(),
           ]);
        } else {
            try {
                DB::table('produks')->where('id',$bodyContent['id'])
                ->update([
                    'nama_produk' => $bodyContent['namaProduk'],
                    'deskripsi_produk' => $bodyContent['deskripsiProduk'],
                    'harga_produk' => $bodyContent['harga'],
                    // 'id_kategori_produk' => $bodyContent['id_kategori_produk'],
                    'path_gambar_produk' => $imageName,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $response = response()->json([
                                'success'=> true,
                                'message'=> 'ok'
                            ]);
            } catch (Exception $e) {
                $response = response()->json([
                                'success'=> false,
                                'message'=> $e->getMessage(),
                            ]);
            }
        }
        return $response;
    }

    public function addData(Request $request)
    {
        $bodyContent = $request->all();
        $image_64 = $request['gambar'];
        $replace = substr($image_64, 0, strpos($image_64, ',')+1); 
        $image = str_replace($replace, '', $image_64); 
        $image = str_replace(' ', '+', $image); 
        $imageName =  $request['fileName'];

        Storage::disk('public')->put($imageName, base64_decode($image));
        $validator = Validator::make($bodyContent,[
            'namaProduk' => 'required',
            'deskripsiProduk' => 'required',
            'harga' => 'required',
            'gambar' => 'required',
            'fileName' => 'required',
        ]);
        if ($validator->fails()) {
            $response = response()->json([
               'success'=> false,
               'message'=> $validator->errors()->all(),
           ]);
        } else {
            try {
                DB::table('produks')->insert([
                    'nama_produk' => $bodyContent['namaProduk'],
                    'deskripsi_produk' => $bodyContent['deskripsiProduk'],
                    'harga_produk' => $bodyContent['harga'],
                    'id_kategori_produk' => $bodyContent['idkategori'],
                    'path_gambar_produk' => $bodyContent['fileName'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
                $response = response()->json([
                                'success'=> true,
                                'message'=> 'ok'
                            ]);
            } catch (Exception $e) {
                $response = response()->json([
                                'success'=> false,
                                'message'=> $e->getMessage(),
                            ]);
            }
        }
       
        return $response;
    }

    public function deleteData(Request $request)
    {
        $bodyContent = $request->all();
        try {
            DB::table('produks')->where('id',$bodyContent['id'])->delete();
            $response = response()->json([
                            'success'=> true,
                            'message'=> 'ok'
                        ]);
        } catch (Exception $e) {
            $response = response()->json([
                            'success'=> false,
                            'message'=> $e->getMessage(),
                        ]);
        }
        return $response;
    }
}
