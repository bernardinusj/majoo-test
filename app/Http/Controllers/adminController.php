<?php

namespace App\Http\Controllers;

use App\Models\produk;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
Use Exception;
use Illuminate\Http\Request;


class adminController extends Controller
{
    public function home()
    {
        $getData = produk::latest()->get();

        return view('vhome',['produks' => $getData]);
    }
    public function index()
    {
        return view('vadminIndex');
    }

    public function addData()
    {
        return view('vadd');
    }

    public function login()
    {
        return view('vlogin');
    }

    public function getData()
    {
        $getData = produk::latest()->get();
        $data = DataTables::of($getData)
                ->addIndexColumn()
                ->addColumn('gambar', function($row){
                    $url=asset("image/$row->path_gambar_produk"); 
                    return '<img src='.$url.' border="0" width="40" class="img-rounded" align="center" />'; 
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('edit-data-produk',$row->id).'" class="edit btn btn-success btn-sm">Edit</a> 
                                    <a onClick="deleteData('.$row->id.')" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->rawColumns(['gambar'])
                ->rawColumns(['action'])
                ->make(true);
        return $data;
    }

    public function editData($id)
    {
        $getProduk = DB::table('produks')
                ->where('id',$id)
                ->get();
        return view('vedit',['produks' => $getProduk]);
    }
}
