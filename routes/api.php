<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apiProdukController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/get-all-produk', [apiProdukController::class, 'getAllProduk']);
Route::get('/get-produk/{id}', [apiProdukController::class, 'getProduk']);
Route::post('/edit-produk', [apiProdukController::class, 'editData'])->name('api-edit-produk');
Route::post('/add-produk', [apiProdukController::class, 'addData'])->name('api-add-produk');
Route::post('/delete-produk', [apiProdukController::class, 'deleteData'])->name('api-delete-produk');



