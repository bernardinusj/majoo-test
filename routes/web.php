<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\adminController;
use App\Http\Controllers\homeController;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/login', function () {
//     return view('vlogin');
// });
Route::get('/', [adminController::class, 'home']);
Route::get('/login', [adminController::class, 'login'])->name('login');
Route::get('/admin', [adminController::class, 'index'])->name('adminHome');
Route::get('/admin/get-data', [adminController::class, 'getData'])->name('getDataProduk');
Route::get('/edit-add-produk', [adminController::class, 'addData'])->name('add-data-produk');
Route::get('/edit-data-produk/{id}', [adminController::class, 'editData'])->name('edit-data-produk');
Route::post('/edit-data-produk-action', [adminController::class, 'editDataAction'])->name('edit-data-action');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
