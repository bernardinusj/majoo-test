<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
<div class="container">
	<h1>LOGIN</h1>
	<div class="panel panel-default">
		<div class="panel-heading">Login</div>
			<div class="panel-body">
				<form action="" method="post">
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="username" class="form-control" />
						<span class="text-danger"></span>
					</div>		
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" class="form-control" />
						<span class="text-danger"></span>
					</div>
					<div class="form-group">
						<input type="submit" name="login" value="Login" class="btn btn-info" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>