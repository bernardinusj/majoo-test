<!DOCTYPE html>
<head>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Open+Sans">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}">
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/media-queries.css') }}">
	<link rel="stylesheet" href="{{ asset('css/carousel.css') }}">
</head>
<body>
    <header class="header">
        Majoo Teknologi Indonesia
    </header>
    <div class="body-page">
        <div class="judul">
            Produk
        </div>
		<div id="carousel-example" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner row w-100 mx-auto" >

			@foreach($produks as $produk)
				<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
					<div class="thumb-wrapper">
						<div class="img-box">
							<img src="{{ url('storage/'.$produk->path_gambar_produk) }}" class="img-fluid" alt="">									
						</div>
						<div class="thumb-content">
							<h4>{{ $produk->nama_produk }}</h4>									
							<p class="item-price"><b>Rp. {{ $produk->harga_produk }}</b></p>
							<p>
								{{ $produk->deskripsi_produk }}
							</p>
							<a href="#" class="btn btn-primary">Beli</a>
						</div>						
					</div>
				</div>
			@endforeach
			</div>
			<a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
    </div>    
	<footer class="footer-container footer">
		<div class="container">
			<div class="row">
				<div class="col">
					2019 &copy; PT Majoo Teknologi Indonesia
				</div>
				
			</div>
		</div>
	</footer>
	<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
	<script src="{{ asset('js/jquery-migrate-3.0.0.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<script src="{{ asset('js/jquery.backstretch.min.js') }}"></script>
	<script src="{{ asset('js/wow.min.js') }}"></script>
	<script src="{{ asset('js/scripts.js') }}"></script>

</body>

