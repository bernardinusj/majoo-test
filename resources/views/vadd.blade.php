<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<title>Add Produk</title>
</head>
<body>
	<h3>Add Produk</h3>
	<a href="/admin"> Kembali</a>
	
	<br/>
	<br/>
	<form >
        <div class="form-group">
            <label for="namaProduk"> Nama Produk</label>
            <input class="form-control" type="text" required="required" id="namaProduk" name="nama" value="">
        </div>
        <div class="form-group">
            <label for="deskripsiProduk"> Deskripsi Produk</label>
            <input class="form-control" id="deskripsiProduk" type="text" required="required" name="deskripsi" value="">
        </div>
        <div class="form-group">
            <label for="harga"> Harga</label>
            <input class="form-control" id="harga" type="number" required="required" name="harga" value="">
        </div>
            Gambar <br> 
        <div class="form-group">
            <b>File Gambar</b><br/>
            <input id="inp" type="file">
            <input id="b64" name="b64" hidden>
            <input id="b64name" name="b64name" hidden>
            <br>
            <img id="img" height="150">
        </div>
	</form>
    <button class="btn btn-primary" id="submit">Submit</button>
    <script type="text/javascript">
        $( document ).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#submit").click(function(){
                $.ajax({
                    url: '{{ route('api-add-produk') }}',
                    type:"POST",
                    data: {
                            'namaProduk': $("#namaProduk").val(),
                            'deskripsiProduk': $("#deskripsiProduk").val(),
                            'harga': $("#harga").val(),
                            'gambar': $("#b64").val(),
                            'fileName': $("#b64name").val(),
                            'idkategori': 1,
                        },
                    success:function(data){
                        console.log(data.success);
                        if (data.success === true) {
                            alert('input berhasil');
                            window.location = '{{ route('adminHome') }}';
                        } else {
                            alert(JSON.stringify(data));
                        }
                    },error:function(data){ 
                        alert(JSON.stringify(data));
                    }
                });
            });
        });
        function readFile() {
            $("#b64name").val(this.files[0].name) ;
            if (!this.files || !this.files[0]) return;
                
            const FR = new FileReader();
                
            FR.addEventListener("load", function(evt) {
                $("#img").attr("src",evt.target.result);
                $("#b64").val(evt.target.result) ;
            }); 
                
            FR.readAsDataURL(this.files[0]);
        }

        document.getElementById("inp").addEventListener("change", readFile);
    </script>
		
</body>
</html>