<!DOCTYPE html>
<html>
<head>
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <a href="{{route('add-data-produk')}}" class="btn btn-success btn-sm">Add</a>
        <div class="col-12 table-responsive">
            <table class="table table-bordered user_datatable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>nama_produk</th>
                        <th>deskripsi_produk</th>
                        <th>harga_produk</th>
                        <th>id_kategori_produk</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th width="100px">gambar</th>
                        <th width="100px">Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  $(function () {
    var table = $('.user_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('getDataProduk') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'nama_produk', name: 'nama_produk'},
            {data: 'deskripsi_produk', name: 'dekripsi_produk'},
            {data: 'harga_produk', name: 'harga_produk'},
            {data: 'id_kategori_produk', name: 'id_kategori_produk'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'gambar', name: 'gambar', orderable: false, searchable: false},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
  function deleteData(id) {
    let text = "anda yakin menghapus data di id "+id+"?";
    if (confirm(text) == true) {
        $.ajax({
            url: '{{ route('api-delete-produk') }}',
            type:"POST",
            data: {
                    'id': id,
                },
            success:function(data){
                console.log(data.success);
                if (data.success === true) {
                    alert('hapus berhasil');
                    window.location = '{{ route('adminHome') }}';
                } else {
                    alert(JSON.stringify(data));
                }
            },error:function(data){ 
                alert(JSON.stringify(data));
            }
        });
    }
  }
</script>
</html>