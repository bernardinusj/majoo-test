<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script> --}}
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    {{-- <meta name="csrf-token" content="{{ csrf_token() }}" /> --}}
	<title>Edit Produk</title>
</head>
<body>
	<h3>Edit Produk</h3>
	<a href="/admin"> Kembali</a>
	
	<br/>
	<br/>
	@foreach($produks as $produk)
	<form >
        <input type="hidden" id="id" name="id" value="{{ $produk->id }}"> <br/>
        <div class="form-group">
            <label for="namaProduk"> Nama Produk</label>
            <input class="form-control" type="text" required="required" id="namaProduk" name="nama" value="{{ $produk->nama_produk }}">
        </div>
        <div class="form-group">
            <label for="deskripsiProduk"> Deskripsi Produk</label>
            <input class="form-control" id="deskripsiProduk" type="text" required="required" name="deskripsi" value="{{ $produk->deskripsi_produk }}">
        </div>
        <div class="form-group">
            <label for="harga"> Harga</label>
            <input class="form-control" id="harga" type="number" required="required" name="harga" value="{{ $produk->harga_produk }}">
        </div>
            Gambar <br> 
            <img src="{{ url('storage/'.$produk->path_gambar_produk) }}" alt="">
            {{-- <img src="{{ url('storage/test.png') }}" alt=""> --}}
        <div class="form-group">
            <b>File Gambar</b><br/>
            <input id="inp" type="file">
            <input id="b64" name="b64" hidden>
            <input id="b64name" name="b64name" hidden>
            <br>
            <img id="img" height="150">
        </div>
	</form>
    <button class="btn btn-primary" id="submit">Submit</button>
	@endforeach
    <script type="text/javascript">
        $( document ).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#submit").click(function(){
                $.ajax({
                    url: '{{ route('api-edit-produk') }}',
                    type:"POST",
                    data: {
                            'id': $("#id").val(),
                            'namaProduk': $("#namaProduk").val(),
                            'deskripsiProduk': $("#deskripsiProduk").val(),
                            'harga': $("#harga").val(),
                            'gambar': $("#b64").val(),
                            'fileName': $("#b64name").val(),
                        },
                    success:function(data){
                        console.log(data.success);
                        if (data.success === true) {
                            alert('input berhasil');
                            window.location = '{{ route('adminHome') }}';
                        } else {
                            alert(JSON.stringify(data));
                        }
                    },error:function(data){ 
                        alert(JSON.stringify(data));
                    }
                });
            });
        });
        function readFile() {
            $("#b64name").val(this.files[0].name) ;
            if (!this.files || !this.files[0]) return;
                
            const FR = new FileReader();
                
            FR.addEventListener("load", function(evt) {
                $("#img").attr("src",evt.target.result);
                $("#b64").val(evt.target.result) ;
            }); 
                
            FR.readAsDataURL(this.files[0]);
        }

        document.getElementById("inp").addEventListener("change", readFile);
    </script>
		
</body>
</html>